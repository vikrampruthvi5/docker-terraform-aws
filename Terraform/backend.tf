terraform {
  backend "s3" {
    bucket = "pvterraformbucket"
    key    = "automation/terraform.tfstate"
    region = "us-east-1"
  }
}