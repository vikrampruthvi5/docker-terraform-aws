import shutil
from os import system
import logging

log = logging.getLogger("test")

# Packaging for common lambda layer creation
shutil.make_archive(base_name="bin/lambda_layer", format="zip", base_dir="lambda_layer", logger=log)